// SPDX-License-Identifier: Apache-2.0
// Copyright: Gabriel Marcano, 2022

#include "mcp4162.h"
#include <spi.h>
#include <tock.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

returncode_t mcp4162_init(void)
{
	returncode_t result = spi_init();
	if (result < 0)
		return result;
	// Chip select is not supported by the SPI virtual in Tock yet
	// Relying on the default CS line being set, which is correct on the
	// HiFive1
	/*result = */spi_set_chip_select(0);
	// if (result < 0)
	//	return result;

	result = spi_set_rate(1000000);
	if (result < 0)
		return result;

	// MCP4162 supports SPI modes (0,0) and (1,1), just use (0,0)
	result = spi_set_phase(false);
	if (result < 0)
		return result;
	result = spi_set_polarity(false);
	return result;
}

int16_t mcp4162_command_data(uint8_t address, uint8_t cmd, uint16_t data)
{
	// MCP4162 2 byte commands
	// To the device:
	// 15 14 13 12 11 10  9  8  7  6  5  4  3  2  1  0
	//  A  A  A  A  C  C  D  D  D  D  D  D  D  D  D  D
	// So 4 bits for address, 2 bits for commands, and 10 bits for data
	// From the device, it depends on the mode, but the top 7 bits are 1 on
	// success, and the 9th bit is 0 on failure.
	// The data is sent with MSB first.
	//
	// When receiving data, only the bottom 9 bits contain data.
	char out_buf[2];
	out_buf[0] =  ( 0x0F & address) << 4;
	out_buf[0] |= ( 0x03 & cmd)     << 2;
	out_buf[0] |= (0x100 & data)    >> 8;
	out_buf[1] = data;

	char in_buf[2];
  	spi_read_write_sync(out_buf, in_buf, 2);

	if ((in_buf[0] & 0xFE) != 0xFE)
		return RETURNCODE_FAIL;

	// Return the bottom 9 bits of data
	int16_t result;
	result = in_buf[1];
	result |= (in_buf[0] & 0x01) << 8;
	return result;
}

returncode_t mcp4162_command(uint8_t address, uint8_t cmd)
{
	// MCP4162 1 byte command
	// To the device:
	// 7  6  5  4  3  2  1  0
	// A  A  A  A  C  C  0  0
	// From the device, bit 1 is 1 on success, 0 on failure. On failure, device
	// will not reset until CS is reset.
	char out_buf[1];
	out_buf[0] =  ( 0x0F & address) << 4;
	out_buf[0] |= ( 0x03 & cmd)     << 2;

	char in_buf[1];
  	spi_read_write_sync(out_buf, in_buf, 1);

	if ((in_buf[0] & 0xFE) != 0xFE)
		return RETURNCODE_FAIL;
	return RETURNCODE_SUCCESS;
}

returncode_t mcp4162_write(uint8_t address, uint16_t data)
{
	int16_t result = mcp4162_command_data(address, 0, data);
	if (result < 0)
		return result;
	return RETURNCODE_SUCCESS;
}

int16_t mcp4162_read(uint8_t address)
{
	return mcp4162_command_data(address, 3, 0);
}

returncode_t mcp4162_increment(uint8_t address)
{
	return mcp4162_command(address, 1);
}

returncode_t mcp4162_decrement(uint8_t address)
{
	return mcp4162_command(address, 2);
}

returncode_t mcp4162_wiper0_write(uint16_t data)
{
	return mcp4162_write(0, data);
}

int16_t mcp4162_wiper0_read(void)
{
	return mcp4162_read(0);
}

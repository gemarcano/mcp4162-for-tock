// SPDX-License-Identifier: Apache-2.0
// Copyright: Gabriel Marcano, 2022

#ifndef MCP4162_H_
#define MCP4162_H_

/** @file */

#include <tock.h>
#include <stdint.h>

/** Initializes SPI peripheral for use with the MCP4162.
 *
 *  Sets chip select to 0, rate to 1MHz, and SPI mode (0,0)
 *
 *  @returns RETURNCODE_SUCCESS on success, anything else on failure.
 */
returncode_t mcp4162_init(void);

/** Sends a 2 byte command to the MCP4162.
 *
 * @param[in] address 4 bit address for register.
 * @param[in] cmd 2 bit command to send.
 * @param[in] data 10 bit data to send.
 *
 * @returns 9 bits of data on success, a returncode_t error code on failure.
 */
int16_t mcp4162_command_data(uint8_t address, uint8_t cmd, uint16_t data);

/** Sends a 1 byte command to the MCP4162.
 *
 * @param[in] address 4 bit address for register.
 * @param[in] cmd 2 bit command to send.
 *
 * @returns RETURNCODE_SUCCESS on success, a returncode_t error code on
 *  failure.
 */
returncode_t mcp4162_command(uint8_t address, uint8_t cmd);

/** Writes data to the specified register.
 *
 * @param[in] address 4 bit address for register.
 * @param[in] data Data to write to register.
 *
 * @returns RETURNCODE_SUCCESS on success, anything else on failure.
 */
returncode_t mcp4162_write(uint8_t address, uint16_t data);

/** Reads the contents of the specified register.
 *
 * @param[in] address 4 bit address for register.
 *
 * @returns 9 bits of data on success, a returncode_t error code on failure.
 */
int16_t mcp4162_read(uint8_t address);

/** Increments the register at the given address.
 *
 * @param[in] address 4 bit address for register.
 *
 * @returns RETURNCODE_SUCCESS on success, anything else on failure.
 */
returncode_t mcp4162_increment(uint8_t address);

/** Decrements the register at the given address.
 *
 * @param[in] address 4 bit address for register.
 *
 * @returns RETURNCODE_SUCCESS on success, anything else on failure.
 */
returncode_t mcp4162_decrement(uint8_t address);

/** Writes data to register 0, associated with volatile wiper 0.
 *
 * @param data Wiper value to set.
 *
 * @returns RETURNCODE_SUCCESS on success, a returncode_t error code on
 *  failure.
 */
returncode_t mcp4162_wiper0_write(uint16_t data);

/** Reads the value of register 0, volatile wiper 0 setting.
 *
 * @returns The value of the register on success, a returncode_t error code on
 *  failure.
 */
int16_t mcp4162_wiper0_read(void);

/** Reads the value of register 5, the status register.
 *
 * @returns the value of the register on success, a returncode_t error code on
 *  failure.
 */
int16_t mcp4162_status(void);

#endif//MCP4162_H_

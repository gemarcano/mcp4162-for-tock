// SPDX-License-Identifier: Apache-2.0
// Copyright: Gabriel Marcano, 2022

#include "mcp4162.h"
#include "cli.h"

#include <console.h>
#include <timer.h>
#include <spi.h>
#include <tock.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(void) {
	mcp4162_init();

	// No point in stuffing this in the limited stack, store it in static
	// memory as there's only one per program
	static struct cli cli = {};
	static bool done = false;
	while (!done)
	{
		if (cli.echo)
		{
			printf("> ");
			fflush(stdout);
		}
		cli_line_buffer* buf = cli_read_line(&cli);
		if (strcmp((const char*)buf, "exit") == 0)
		{
			done = true;
			continue;
		}
		else if (strcmp((const char*)buf, "?") == 0)
		{
			printf("mcp4142 controller\r\n");
		}
		else if (strcmp((const char*)buf, "read") == 0)
		{
			int16_t dat = mcp4162_read(0);
			printf("%i\r\n", dat);
		}
		else if (strncmp((const char*)buf, "write ", 6) == 0)
		{
			const char *cur = strtok((char*)buf, " ");
			cur = strtok(NULL, " ");
			unsigned long dat = strtoul(cur, NULL, 10);

			int res = mcp4162_write(0, dat);
			printf("%i\r\n", res);
		}
		else if (strcmp((const char*)buf, "history") == 0)
		{
			size_t max = ring_buffer_in_use(&cli.history);
			for (size_t i = 0; i < max; ++i)
			{
				printf("%d %s\r\n", i+1, (char*)ring_buffer_get(&cli.history, max-1-i));
			}
		}
		else if (strcmp((const char*)buf, "echo") == 0)
		{
			cli.echo = !cli.echo;
		}
		else
		{
			printf("invalid command\r\n");
		}
	}
	return 0;
}
